﻿using System;
using UnityEngine;

namespace Assets
{
    public class PlayerPickup : MonoBehaviour
    {

        public AudioClip CoinSound;
        private AudioSource m_audioSource;
        public event EventHandler<EventArgs> Pickup;

        void Start ()
        {
            m_audioSource = GetComponent<AudioSource>();
        }
	
        void Update () {
	
        }

        void OnTriggerEnter2D(Collider2D coll)
        {
            if (coll.gameObject.tag == "Star")
            {
                m_audioSource.clip = CoinSound;
                m_audioSource.Play();
                Destroy(coll.gameObject);
                OnPickup();
            }
        }

        protected virtual void OnPickup()
        {
            EventHandler<EventArgs> handler = Pickup;
            if (handler != null) handler(this, EventArgs.Empty);
        }

    }
}
