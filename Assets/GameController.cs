﻿using System;
using System.Collections.Generic;
using Assets;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{

    public Text ScoreText;
    public int m_score;
    private ICollection<PlayerPickup> m_pickups;

    void Start()
    {
        m_pickups = FindObjectsOfType<PlayerPickup>();
        AddPickupListeners();
        DisplayScore();
    }

    private void AddPickupListeners()
    {
        foreach (var pickup in m_pickups)
        {
            pickup.Pickup += Pickup;
        }
    }

    private void Pickup(object sender, EventArgs eventArgs)
    {

        m_score++;
        DisplayScore();
    }

    private void DisplayScore()
    {
        ScoreText.text = "Score:" + m_score;
        
    }


}
