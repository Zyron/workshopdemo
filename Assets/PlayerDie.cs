﻿using UnityEngine;
using System.Collections;

public class PlayerDie : MonoBehaviour
{

    public AudioClip DieClip;
    private AudioSource m_audioSource;
    private bool m_isDead;
    void Start()
    {
        m_audioSource = GetComponent<AudioSource>();
    }
	void Update () {
	    if (transform.position.y < -10 && !m_isDead)
	    {
	        m_isDead = true;
	        StartCoroutine(EndGame());
	    }
            
	}

    private IEnumerator EndGame()
    {
        m_audioSource.clip = DieClip;
        m_audioSource.Play();
        yield return new WaitForSeconds(DieClip.length);
        Application.LoadLevel(0);
    }
}
